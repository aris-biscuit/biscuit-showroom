﻿using System;
using System.Configuration;
using System.Security.Cryptography.X509Certificates;
using System.Web.Mvc;
using System.Web.Security;
using ShowRoom.Models;

namespace ShowRoom.Controllers
{
    public class AccountController : Controller
    {
        public ActionResult Login(string returnUrl)
        {
            return View(new LoginViewModel { ReturnUrl = returnUrl });
        }

        [HttpPost]
        public ActionResult Login(LoginViewModel model)
        {
            if (ModelState.IsValid)
            {
                string folderPath = ConfigurationManager.AppSettings["ListDirectory"];

                var lines = System.IO.File.ReadAllText($"{folderPath}\\credentials.txt").Split(new[] { "\r\n" }, StringSplitOptions.RemoveEmptyEntries);

                foreach (var folder in lines)
                {
                    var values = folder.Split(';');
                    if (values[1].Equals(model.Email, StringComparison.OrdinalIgnoreCase) &&
                        values[2].Equals(model.Password))
                    {
                        FormsAuthentication.SetAuthCookie(values[0], true);
                        if (!string.IsNullOrEmpty(model.ReturnUrl))
                        {
                            return Redirect(model.ReturnUrl);
                        }
                        return RedirectToAction("Index", "Home");
                    }
                }
                ModelState.AddModelError("", "Invalid login attempt.");
            }
            return View(model);
        }

        public ActionResult Logout()
        {
            FormsAuthentication.SignOut();
            return RedirectToAction("Index", "Home");
        }
    }
}