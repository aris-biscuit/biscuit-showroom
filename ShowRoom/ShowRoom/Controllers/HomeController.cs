﻿using System.Configuration;
using System.Configuration.Internal;
using System.IO;
using System.Linq;
using System.Web.Mvc;
using ShowRoom.Models;

namespace ShowRoom.Controllers
{
    public class HomeController : Controller
    {
        [Authorize]
        public ActionResult Index(string folder)
        {
            var model = new HomeIndexViewModel
            {
                Folder = folder,
                Root = ConfigurationManager.AppSettings["ListDirectory"] + "\\" + User.Identity.Name
            };

            var root = model.Root;

            if (!string.IsNullOrEmpty(folder))
            {
                root = $"{root}\\{folder}";
            }
            model.Folders = Directory.GetDirectories(root).Select(Path.GetFullPath).ToList();
            model.Files = Directory.GetFiles(root).Select(Path.GetFileName).ToList();

            return View(model);
        }

        public ActionResult File(string path)
        {
            var html = System.IO.File.ReadAllText($"{ConfigurationManager.AppSettings["ListDirectory"] + "\\" + User.Identity.Name}\\{path}");
            return Content(html);
        }

    }
}