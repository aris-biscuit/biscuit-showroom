﻿using System.Collections.Generic;

namespace ShowRoom.Models
{
    public class HomeIndexViewModel
    {
        public string Folder { get; set; }
        
        public string Root { get; set; }

        public List<string> Folders { get; set; }

        public List<string> Files { get; set; }
        
    }
}